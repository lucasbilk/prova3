
import utfpr.ct.dainf.pratica.Poligonal;
import utfpr.ct.dainf.pratica.PoligonalFechada;
import utfpr.ct.dainf.pratica.PontoXZ;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica {

    public static void main(String[] args) { 
        
        PontoXZ ponto1 = new PontoXZ(-3,2);
        PontoXZ ponto2 = new PontoXZ(-3,6);
        PontoXZ ponto3 = new PontoXZ(0,2);
        
        PoligonalFechada poligonal2  = new PoligonalFechada(); 
        poligonal2.addFinal(ponto1);
        poligonal2.addFinal(ponto2);
        poligonal2.addFinal(ponto3);
        
        System.out.println("Comprimento da poligonal = " + poligonal2.getComprimento());      
    }
    
}
