package utfpr.ct.dainf.pratica;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <T> Tipo do ponto
 */
public class Poligonal<T extends Ponto2D> {
    public final List<T> vertices = new ArrayList<>();   
    
    public Poligonal(){
        
    }
    
    public void addFinal(T ponto){
        this.vertices.add(ponto);
    }
       
    public void set(int i, T p){
        if(i == this.vertices.size()){
            this.vertices.add(i, p);
        }else if(i<0 || i>this.vertices.size()){
            throw new RuntimeException("set("+i+"): índice inválido");
        }else{
            this.vertices.add(i,p);
        }
    }
    
    public int  getN(){ 
        return this.vertices.size();
    }
    
    public T get(int i){
        if(i<0 || i>this.vertices.size()){
            throw new RuntimeException("get("+i+"): índice inválido");
        }
            return this.vertices.get(i);
    }
    
    public double getComprimento(){
        double comprimento = 0;
 
        ListIterator<T> it = this.vertices.listIterator();       
        
        while (it.hasNext()) {
        T ponto = it.next();
            if(it.hasNext()){
                comprimento += ponto.dist(it.next());
                ponto = it.previous();
            }   
        }
        return comprimento;
    }
}
