/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

import java.util.ListIterator;

/**
 *
 * @author a1249568
 * @param <T> Tipo do ponto
 */
public class PoligonalFechada<T extends Ponto2D> extends Poligonal{
    
    public PoligonalFechada() {
        super();
    }
    
    @Override
    public double getComprimento(){
        double comprimento = 0;
 
        ListIterator<T> it = this.vertices.listIterator();    
        
        T pontoInicial = it.next();
        T ponto = it.previous();
               
        while (it.hasNext()) {
        ponto = it.next();
            if(it.hasNext()){
                comprimento += ponto.dist(it.next());
                ponto = it.previous();
            }   
        }
        comprimento += ponto.dist(pontoInicial);
        return comprimento;
    }
    
    
}
